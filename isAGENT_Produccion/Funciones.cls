VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Funciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
"GetPrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpDefault _
As String, ByVal lpReturnedString As String, ByVal _
nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
"WritePrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpstring _
As Any, ByVal lpFileName As String) As Long

Public Function sGetIni(SIniFile As String, SSection As String, SKey _
As String, SDefault As String) As String
    
    Const MaxLen = 10000
    
    Dim sTemp As String * MaxLen
    Dim NLength As Integer
    
    sTemp = Space$(MaxLen)
    NLength = GetPrivateProfileString(SSection, SKey, SDefault, sTemp, MaxLen - 1, SIniFile)
    
    sGetIni = Left$(sTemp, NLength)
    
End Function

Public Function sWriteIni(SIniFile As String, SSection As String, SKey _
        As String, SData As String) As String
    Dim NLength As Integer
    
    NLength = WritePrivateProfileString(SSection, SKey, SData, SIniFile)
End Function

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = "" Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Sub Ejecutar()
    
    Setup = (App.Path & "\Setup.ini")
    
    GenerarSolicitudDeCompra = Val(sGetIni(Setup, "SolicitudCompra", "Generar", "1")) = 1
    GenerarSolicitudDeTraslado = Val(sGetIni(Setup, "SolicitudTraslado", "Generar", "1")) = 1
    
    If GenerarSolicitudDeCompra Then
        Call GenerarSDCxOrden
    End If
    
    If GenerarSolicitudDeTraslado Then
        Call GenerarSDTxOrden
    End If
    
End Sub

Public Sub GenerarSDCxOrden()
    
    Dim mSQLPaso1, mSQLPaso2, mSQLPaso3, mSQLPaso4, mSQLPaso5, _
    mSQLFinal
    
    On Error GoTo Error1
    
    Dim Trans As Boolean
    
    Trans = False
    
    Dim DiasAnticipacion As Integer
    DiasAnticipacion = Val(sGetIni(Setup, "SolicitudCompra", "DiasAnticipacion", "7"))
    If DiasAnticipacion <= 0 Then DiasAnticipacion = 1
    
    Dim PorcentajeExistencia As Double
    PorcentajeExistencia = Val(sGetIni(Setup, "SolicitudCompra", "PorcentajeExistencia", "100"))
    If PorcentajeExistencia < 0 Or PorcentajeExistencia > 100 Then PorcentajeExistencia = 100
    
    Dim ConsideraStockMinimo As Boolean
    ConsideraStockMinimo = Val(sGetIni(Setup, "SolicitudCompra", "ConsideraStockMinimo", "0")) = 1
    
    Dim CriterioDepositos As String, SQLDepositos As String
    CriterioDepositos = UCase(sGetIni(Setup, "SolicitudCompra", "FiltrarProductosPorDeposito", "TODOS"))
    
    Dim DepositoSDC As String
    DepositoSDC = QuitarComillasSimples(UCase(sGetIni(Setup, "SolicitudCompra", "DepositoSDC", Empty)))
    
    Select Case CriterioDepositos
        
        Case "TODOS"
            SQLDepositos = Empty
            
        Case Else
            
            If InStr(CriterioStatus, "SOLO(") > 0 Then
            
                CriterioDepositos = Replace(CriterioDepositos, "SOLO(", Empty)
                CriterioDepositos = Replace(CriterioDepositos, ")", Empty)
                
                ArrStatus = Split(CriterioDepositos, "|")
                
                SQLDepositos = " AND c_CodDeposito IN ("
                SqlTempDataIn = ""
                
                For i = 0 To UBound(ArrStatus)
                    SqlTempDataIn = SqlTempDataIn & IIf(i < UBound(ArrStatus), _
                    "'" & ArrStatus(i) & "', ", "'" & ArrStatus(i) & "'")
                Next i
                
                SQLDepositos = SQLDepositos & SqlTempDataIn & ")"
            
            ElseIf InStr(CriterioStatus, "EXCEPTO(") > 0 Then
            
                CriterioDepositos = Replace(CriterioDepositos, "EXCEPTO(", Empty)
                CriterioDepositos = Replace(CriterioDepositos, ")", Empty)
                
                ArrStatus = Split(CriterioStatus, "|")
                
                SQLDepositos = " AND c_CodDeposito NOT IN ("
                SqlTempDataIn = ""
                
                For i = 0 To UBound(ArrStatus)
                    SqlTempDataIn = SqlTempDataIn & IIf(i < UBound(ArrStatus), _
                    "'" & ArrStatus(i) & "', ", "'" & ArrStatus(i) & "'")
                Next i
                
                SQLDepositos = SQLDepositos & SqlTempDataIn & ")"
                
            Else
            
                SQLDepositos = ""
            
            End If
            
    End Select
    
    Call ConectarBDD
    
    ENT.BDD.Execute "USE VAD10"
    
    If Not ObtenerMonedaPredeterminada(ENT.BDD) Then
        End
    End If
    
    Set RsDeposito = ENT.BDD.Execute("SELECT * FROM MA_DEPOSITO " & _
    "WHERE c_CodDeposito = '" & DepositoSDC & "' ")
    
    If RsDeposito.EOF Then
        LogContent = FormatDateTime(Now, vbGeneralDate) & " - El dep�sito indicado para la solicitud de compra no existe o no est� definido."
        LogFile LogContent
        End
    End If
    
    RsDeposito.Close
    
    mSQLPaso1 = _
    "IF EXISTS(SELECT * FROM TempDB.INFORMATION_SCHEMA.TABLES " & vbNewLine & _
    "WHERE TABLE_NAME LIKE '%#Build_SDC%') " & vbNewLine & _
    "   DROP TABLE #Build_SDC " & vbNewLine
    
    mSQLPaso2 = _
    ";WITH ORDENES AS ( " & vbNewLine & _
    "SELECT DATEDIFF(DAY, CAST(GETDATE() AS DATE), CAST(d_Fecha_Produccion AS DATE)) AS DiasRestantesCompra, OPR.* " & vbNewLine & _
    "FROM MA_ORDEN_PRODUCCION OPR LEFT JOIN MA_CONTROL_AGENTE_ORDEN_PRODUCCION AGT " & vbNewLine & _
    "ON OPR.c_CodLocalidad = AGT.Localidad AND OPR.c_Documento = AGT.Documento_OPR " & vbNewLine & _
    "WHERE c_Status IN ('DPE') AND AGT.Documento_SDC IS NULL " & vbNewLine & _
    "), ProductosEvaluar AS ( " & vbNewLine & _
    "SELECT TR.*, PRODUCTO.nu_StockMin, PRODUCTO.nu_StockMax, " & vbNewLine & _
    "PRODUCTO.n_Impuesto1, PRODUCTO.n_Impuesto2, PRODUCTO.n_Impuesto3, PRODUCTO.Cant_Decimales AS CantDec, " & vbNewLine & _
    "MA.d_Fecha_Produccion, MA.c_Usuario AS CodUsuarioOPR, MA.c_CodMoneda AS MonedaDoc, MA.n_FactorCambio AS FactorDoc " & vbNewLine & _
    "FROM ORDENES MA INNER JOIN TR_ORDEN_PRODUCCION TR " & vbNewLine & _
    "ON MA.c_Documento = TR.c_Documento " & vbNewLine & _
    "AND MA.c_CodLocalidad = TR.c_CodLocalidad " & vbNewLine & _
    "INNER JOIN MA_PRODUCTOS PRODUCTO " & vbNewLine & _
    "ON TR.c_CodArticulo = PRODUCTO.c_Codigo " & vbNewLine & _
    "WHERE DiasRestantesCompra > 0 And DiasRestantesCompra <= " & DiasAnticipacion & " And b_Producir = 0 " & vbNewLine & _
    "), DepoProd AS ( " & vbNewLine & _
    "SELECT ROUND(isNULL((DP.n_Cantidad - CASE WHEN DP.n_Cant_Comprometida " & vbNewLine & _
    "> 0 THEN DP.n_Cant_Comprometida ELSE 0 END), 0), 8, 0) AS Existencia, " & vbNewLine & _
    "DP.c_CodDeposito, PRO.* FROM ProductosEvaluar PRO LEFT JOIN MA_DEPOPROD DP " & vbNewLine & _
    "ON PRO.c_CodArticulo = DP.c_CodArticulo " & vbNewLine
    
    mSQLPaso2 = mSQLPaso2 & _
    "), RequeridosPorDocumento AS ( " & vbNewLine & _
    "SELECT c_CodLocalidad, c_Documento, c_CodArticulo, CantDec, d_Fecha_Produccion, MonedaDoc, FactorDoc, " & vbNewLine & _
    "ROUND(SUM(n_Cantidad), CantDec, 0) AS Requeridos, " & vbNewLine & _
    "ROUND((isNULL(SUM(Existencia), 0) * (" & FormatNumber(PorcentajeExistencia, 2) & " / 100.0))" & IIf(ConsideraStockMinimo, " - nu_StockMin", Empty) & ", CantDec, 0) AS Existencia, " & vbNewLine & _
    "ROW_NUMBER() OVER (PARTITION BY c_CodArticulo ORDER BY d_Fecha_Produccion, c_CodArticulo) AS Prioridad, " & vbNewLine & _
    "ROW_NUMBER() OVER (ORDER BY c_CodArticulo) AS RowID " & vbNewLine & _
    "FROM DEPOPROD " & vbNewLine & _
    "WHERE 1 = 1 " & SQLDepositos & vbNewLine & _
    "GROUP BY c_CodLocalidad, c_Documento, c_CodArticulo, CantDec, d_Fecha_Produccion, nu_StockMin, MonedaDoc, FactorDoc " & vbNewLine & _
    "), CALC AS ( " & vbNewLine & _
    "SELECT " & vbNewLine & _
    "RowID, c_CodLocalidad, c_Documento, c_CodArticulo, d_Fecha_Produccion, Requeridos, Existencia, Prioridad, " & vbNewLine & _
    "CASE WHEN Existencia > Requeridos THEN ROUND(Existencia - Requeridos, CantDec, 0) ELSE 0 END AS ExistenciaRestante, " & vbNewLine & _
    "CASE WHEN (Requeridos > Existencia) THEN ROUND(Requeridos - Existencia, CantDec, 0) ELSE 0 END AS Solicitar " & vbNewLine & _
    "FROM RequeridosPorDocumento WHERE Prioridad = 1 " & vbNewLine & _
    "UNION ALL " & vbNewLine
    
    mSQLPaso2 = mSQLPaso2 & _
    "SELECT " & vbNewLine & _
    "RequeridosPorDocumento.RowID, RequeridosPorDocumento.c_CodLocalidad, RequeridosPorDocumento.c_Documento, RequeridosPorDocumento.c_CodArticulo, RequeridosPorDocumento.d_Fecha_Produccion, " & vbNewLine & _
    "RequeridosPorDocumento.Requeridos, RequeridosPorDocumento.Existencia, RequeridosPorDocumento.Prioridad, " & vbNewLine & _
    "(CASE WHEN CALC.ExistenciaRestante > 0 THEN ROUND(CALC.ExistenciaRestante - " & vbNewLine & _
    "(CASE WHEN CALC.ExistenciaRestante > RequeridosPorDocumento.Requeridos THEN " & vbNewLine & _
    "RequeridosPorDocumento.Requeridos ELSE CALC.ExistenciaRestante END) " & vbNewLine & _
    ", RequeridosPorDocumento.CantDec, 0) ELSE 0 END) AS ExistenciaRestante, " & vbNewLine & _
    "CASE WHEN CALC.ExistenciaRestante > 0  THEN " & vbNewLine & _
    "CASE WHEN (RequeridosPorDocumento.Requeridos > CALC.ExistenciaRestante) THEN " & vbNewLine & _
    "ROUND(RequeridosPorDocumento.Requeridos - CALC.ExistenciaRestante, CantDec, 0) ELSE 0 END " & vbNewLine & _
    "ELSE RequeridosPorDocumento.Requeridos END AS Solicitar " & vbNewLine & _
    "FROM CALC INNER JOIN RequeridosPorDocumento " & vbNewLine & _
    "ON RequeridosPorDocumento.RowID = CALC.RowID + 1 AND RequeridosPorDocumento.Prioridad > 1 " & vbNewLine & _
    ") , BUILD_SDC_1 AS ( " & vbNewLine & _
    "SELECT c_Documento AS Doc_ID, ROW_NUMBER() OVER (ORDER BY c_Documento) AS SDC_ID " & vbNewLine & _
    "FROM CALC GROUP BY c_Documento " & vbNewLine
    
    mSQLPaso2 = mSQLPaso2 & _
    "), BUILD_SDC_2 AS ( " & vbNewLine & _
    "SELECT CorrelativoSDC = RIGHT('0000000000000000' + CAST((nu_Valor + SDC_ID) AS NVARCHAR(MAX)), 9), " & vbNewLine & _
    "CALC.*, SDC.SDC_ID " & vbNewLine & _
    "FROM VAD10..MA_CORRELATIVOS CROSS JOIN CALC INNER JOIN BUILD_SDC_1 SDC " & vbNewLine & _
    "ON CALC.c_Documento = SDC.Doc_ID " & vbNewLine & _
    "WHERE cu_Campo = 'SOLICITUD_COMPRA' " & vbNewLine & _
    ") SELECT SDC2.SDC_ID, SDC2.CorrelativoSDC, SDC2.ExistenciaRestante, SDC2.Solicitar, Items.* " & vbNewLine & _
    "INTO #Build_SDC FROM BUILD_SDC_2 SDC2 " & vbNewLine & _
    "INNER JOIN ProductosEvaluar Items " & vbNewLine & _
    "ON Items.c_CodArticulo = SDC2.c_CodArticulo " & vbNewLine & _
    "AND Items.c_CodLocalidad = SDC2.c_CodLocalidad " & vbNewLine & _
    "AND Items.c_Documento = SDC2.c_Documento " & vbNewLine & _
    "WHERE Solicitar > 0 " & vbNewLine & _
    "ORDER BY SDC2.RowID " & vbNewLine & _
    "OPTION (MAXRECURSION 9999) " & vbNewLine & _
    "; " & vbNewLine
    
    mSQLPaso3 = _
    "DECLARE @CantSDC INT " & vbNewLine & _
    "SET @CantSDC = (SELECT isNULL(COUNT(*), 0) AS Cant FROM #Build_SDC) " & vbNewLine
    
    mSQLPaso3 = mSQLPaso3 & _
    "IF (@CantSDC > 0) " & vbNewLine & _
    "BEGIN " & vbNewLine & _
    "" & vbNewLine & _
    "    BEGIN TRANSACTION " & vbNewLine & _
    "    " & vbNewLine & _
    "    INSERT INTO TR_SOLICITUDCOMPRAS " & vbNewLine & _
    "    (c_Documento, c_CodArticulo, n_Cantidad, n_COSTO, n_subtotal, n_porcentajeimp, n_porcentajeimp1, n_porcentajeimp2, " & vbNewLine & _
    "    c_codprincipal, n_COSTOORIGINAL, N_PROD_EXT, c_descripcion, TR_SOLICITUD, cs_codlocalidad, ns_CantidadEmpaque, N_CANTIDADFAC) " & vbNewLine & _
    "    SELECT CorrelativoSDC, SDC.c_CodArticulo, Solicitar, n_Costo, ROUND(Solicitar * n_Costo, 8, 0), n_Impuesto1, n_Impuesto2, n_Impuesto3, " & vbNewLine & _
    "    c_CodArticulo, n_CostoOriginal, n_Prod_Ext, '', '0', c_CodLocalidad, ns_CantidadEmpaque, 0 " & vbNewLine & _
    "    FROM #Build_SDC SDC ORDER BY CorrelativoSDC, SDC.c_CodArticulo " & vbNewLine
    
    mSQLPaso3 = mSQLPaso3 & _
    "    INSERT INTO MA_SOLICITUDCOMPRAS " & vbNewLine & _
    "    (c_DOCUMENTO, d_FECHA, c_status, c_CODPROVEEDOR, c_CODLOCALIDAD, c_codmoneda, n_FACTORCAMBIO, c_OBSERVACION, " & vbNewLine & _
    "    N_SUBTOTAL, N_IMPUESTO, N_TOTAL, C_USUARIO, cs_codlocalidad, c_CODDEPOSITO) " & vbNewLine & _
    "    SELECT TOTALES.Doc, CAST(GETDATE() AS DATE), 'DPE', '', TOTALES.Loc, TOTALES.MonedaDoc, TOTALES.FactorDoc, " & vbNewLine & _
    "    'SOLICITUD DE COMPRA GENERADA AUTOMATICAMENTE POR AGENTE DE ORDENES DE PRODUCCI�N N� ' + TOTALES.DocOrigen, " & vbNewLine & _
    "    TOTALES.SubDoc, TOTALES.ImpuestoDoc, TOTALES.TotalDoc, TOTALES.CodUsuarioOPR, TOTALES.Loc, '" & DepositoSDC & "' FROM " & vbNewLine & _
    "    (SELECT TR2.cs_CodLocalidad AS Loc, TR2.c_Documento AS Doc, SDC2.c_Documento AS DocOrigen, SDC2.CodUsuarioOPR, SDC2.MonedaDoc, SDC2.FactorDoc, " & vbNewLine & _
    "    ROUND(SUM(TR2.n_Subtotal), 8, 0) AS SubDoc, " & vbNewLine & _
    "    ROUND(SUM(TR2.n_Subtotal * ((n_porcentajeimp + n_porcentajeimp1 + n_porcentajeimp2) / 100)), 8, 0) AS ImpuestoDoc, " & vbNewLine & _
    "    ROUND(Sum(TR2.n_Subtotal * (1 + (n_porcentajeimp + n_porcentajeimp1 + n_porcentajeimp2) / 100)), 8, 0) As TotalDoc " & vbNewLine & _
    "    FROM TR_SOLICITUDCOMPRAS TR2 INNER JOIN #Build_SDC SDC2 " & vbNewLine & _
    "    ON TR2.c_DOCUMENTO = SDC2.CorrelativoSDC " & vbNewLine & _
    "    AND TR2.c_CODARTICULO = SDC2.c_CodArticulo " & vbNewLine & _
    "    GROUP BY TR2.cs_CodLocalidad, TR2.c_DOCUMENTO, SDC2.c_Documento, SDC2.CodUsuarioOPR, SDC2.MonedaDoc, SDC2.FactorDoc) TOTALES " & vbNewLine & _
    "    ORDER BY TOTALES.Doc " & vbNewLine
    
    mSQLPaso3 = mSQLPaso3 & _
    "" & vbNewLine & _
    "    UPDATE VAD10..MA_CORRELATIVOS SET nu_Valor = (nu_Valor + @CantSDC)  WHERE cu_Campo = 'SOLICITUD_COMPRA' " & vbNewLine & _
    "" & vbNewLine & _
    "    INSERT INTO MA_CONTROL_AGENTE_ORDEN_PRODUCCION " & vbNewLine & _
    "    (Localidad, Documento_OPR, Documento_SDC, Documento_SDT) " & vbNewLine & _
    "    SELECT c_CodLocalidad, c_Documento, NULL, NULL " & vbNewLine & _
    "    FROM ( " & vbNewLine & _
    "    SELECT c_CodLocalidad, c_Documento FROM #Build_SDC " & vbNewLine & _
    "    GROUP BY c_CodLocalidad, c_Documento) OPR " & vbNewLine & _
    "    LEFT JOIN MA_CONTROL_AGENTE_ORDEN_PRODUCCION AGT " & vbNewLine & _
    "    ON OPR.c_CodLocalidad = AGT.Localidad " & vbNewLine & _
    "    AND OPR.c_Documento = AGT.Documento_OPR " & vbNewLine & _
    "    WHERE AGT.Documento_OPR IS NULL " & vbNewLine & _
    "    ORDER BY c_CodLocalidad, c_Documento " & vbNewLine
    
    mSQLPaso3 = mSQLPaso3 & _
    "" & vbNewLine & _
    "    Update MA_CONTROL_AGENTE_ORDEN_PRODUCCION " & vbNewLine & _
    "    Set Documento_SDC = CorrelativoSDC " & vbNewLine & _
    "    FROM ( " & vbNewLine & _
    "    SELECT c_CodLocalidad, c_Documento, CorrelativoSDC FROM #Build_SDC " & vbNewLine & _
    "    GROUP BY c_CodLocalidad, c_Documento, CorrelativoSDC) OPR " & vbNewLine & _
    "    INNER JOIN MA_CONTROL_AGENTE_ORDEN_PRODUCCION AGT " & vbNewLine & _
    "    ON OPR.c_CodLocalidad = AGT.Localidad " & vbNewLine & _
    "    AND OPR.c_Documento = AGT.Documento_OPR " & vbNewLine & _
    "     " & vbNewLine & _
    "    COMMIT TRANSACTION " & vbNewLine & _
    "" & vbNewLine & _
    "END " & vbNewLine
    
    ENT.BDD.CommandTimeout = 0
    
    Dim TInicio As Date, TFin As Date
    
    'ENT.BDD.BeginTrans
    
    TInicio = Now
    
    'Trans = True
    
    ENT.BDD.Execute mSQLPaso1
    ENT.BDD.Execute mSQLPaso2
    ENT.BDD.Execute mSQLPaso3
    
    'ENT.BDD.CommitTrans
    
    TFin = Now
    
    Debug.Print "Tiempo de actualizacion: " & FormatDateTime(CDate(TFin - TInicio), vbLongTime)
    
    Exit Sub
    
Error1:
    
    If Trans Then
        ENT.BDD.RollbackTrans
    End If
    
    LogContent = FormatDateTime(Now, vbGeneralDate) & " - Error al Generar solicitudes de Compra: " & Err.Description & " " & "(" & Err.Number & ")[" & Err.Source & "]"
    LogFile LogContent
    
End Sub

Private Sub ConectarBDD()

    On Error GoTo ErrHandler
    
    Dim Setup As String
    
    Setup = App.Path & "\Setup.ini"
    
    Dim SRV_LOCAL As String, SRV_REMOTE As String, PROVIDER_LOCAL As String, _
    UserDB As String, UserPwd As String, NewUser As String, NewPassword As String
    
    'BUSCAR VALORES
    
    SRV_LOCAL = sGetIni(Setup, "Server", "srv_local", "?")
    
    If SRV_LOCAL = "?" Then
        Call MsgBox("El servidor local no se encuentra configurado.", vbCritical, "Error")
        End
    End If
    
    SRV_REMOTE = sGetIni(Setup, "Server", "srv_remote", "?")
    
    If SRV_REMOTE = "?" Then
        Call MsgBox("El servidor remoto no se encuentra configurado.", vbCritical, "Error")
        End
    End If
    
    PROVIDER_LOCAL = sGetIni(Setup, "Proveedor", "Proveedor", "?")
    
    If PROVIDER_LOCAL = "?" Then
        Call MsgBox("El proveedor de servicio local no se encuentra configurado.", vbCritical, "Error")
        End
    End If
    
    UserDB = sGetIni(Setup, "Server", "User", "?")
    
    If UserDB = "?" Then
        UserDB = "SA"
    End If
    
    UserPwd = sGetIni(Setup, "Server", "Password", "?")
    
    If UserPwd = "?" Then
        UserPwd = ""
    End If
    
    If Not (UCase(UserDB) = "SA" And Len(UserPwd) = 0) Then
        
        Dim mClsTmp As Object
        
        Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
        
        If Not mClsTmp Is Nothing Then
            UserDB = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, UserDB)
            UserPwd = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, UserPwd)
        End If
        
    End If
    
Retry:
    
    'INICIO CONEXION
    
    SafeItemAssign ENT.BDD.Properties, "Prompt", adPromptNever
    
    If ENT.BDD.State = adStateOpen Then ENT.BDD.Close
    
    ENT.BDD.ConnectionString = "Provider= " & PROVIDER_LOCAL & ";Persist Security Info=True;User ID=" & UserDB & ";Password=" & UserPwd & ";Initial Catalog=VAD10;Data Source=" & SRV_REMOTE
    
    ENT.BDD.CommandTimeout = 0
    
    ENT.BDD.Open
    
    ConectarBaseDatos = True
    
    Exit Sub
    
ErrHandler:
    
    mErrDesc = Err.Description
    mErrNumber = Err.Number
    mErrSrc = Err.Source
    
    If mErrNumber = -2147217843 Then
        
        Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
        
        If mClsTmp Is Nothing Then GoTo UnhandledErr
        
        TmpVar = mClsTmp.RequestAccess(gCodProducto, gNombreProducto, gPK)
        
        If Not IsEmpty(TmpVar) Then
            UserDB = TmpVar(0): UserPwd = TmpVar(1)
            NewUser = TmpVar(2): NewPassword = TmpVar(3)
            sWriteIni Setup, "Server", "User", NewUser
            sWriteIni Setup, "Server", "Password", NewPassword
            Resume Retry
        End If
        
        Set mClsTmp = Nothing
        
    End If
    
UnhandledErr:
    
    'Call MsgBox(Err.Description, vbCritical, "Error")
    
    LogContent = FormatDateTime(Now, vbGeneralDate) & " - Error al conectar a la BD: " & mErrDesc & " " & "(" & mErrNumber & ")[" & mErrSrc & "]"
    LogFile LogContent
    End
    
End Sub

Public Sub GenerarSDTxOrden()
    
    Dim mSQLPaso1, mSQLPaso2, mSQLPaso3, mSQLPaso4, mSQLPaso5, _
    mSQLFinal
    
    On Error GoTo Error1
    
    Dim Trans As Boolean
    
    Trans = False
    
    Dim DiasAnticipacion As Integer
    DiasAnticipacion = Val(sGetIni(Setup, "SolicitudTraslado", "DiasAnticipacion", "2"))
    If DiasAnticipacion <= 0 Then DiasAnticipacion = 1
    
    Dim PorcentajeExistencia As Double
    PorcentajeExistencia = Val(sGetIni(Setup, "SolicitudTraslado", "PorcentajeExistencia", "100"))
    If PorcentajeExistencia < 0 Or PorcentajeExistencia > 100 Then PorcentajeExistencia = 100
    
    Dim ConsideraStockMinimo As Boolean
    ConsideraStockMinimo = Val(sGetIni(Setup, "SolicitudTraslado", "ConsideraStockMinimo", "0")) = 1
    
    Dim CriterioDepositos As String, SQLDepositos As String
    CriterioDepositos = UCase(sGetIni(Setup, "SolicitudTraslado", "FiltrarProductosPorDeposito", "TODOS"))
    
    Dim DepositoOrigen As String
    DepositoOrigen = QuitarComillasSimples(UCase(sGetIni(Setup, "SolicitudTraslado", "DepositoOrigen", Empty)))
    
    Dim DepositoDestino As String
    DepositoDestino = QuitarComillasSimples(UCase(sGetIni(Setup, "SolicitudTraslado", "DepositoDestino", Empty)))
    
    Dim ConsiderarExistencias As Boolean, mCantidadSolicitar As String
    ConsiderarExistencias = Val(sGetIni(Setup, "SolicitudTraslado", "ConsiderarExistencias", "0")) = 1
    
    Select Case CriterioDepositos
        
        Case "TODOS"
            SQLDepositos = Empty
            
        Case Else
            
            If InStr(CriterioStatus, "SOLO(") > 0 Then
            
                CriterioDepositos = Replace(CriterioDepositos, "SOLO(", Empty)
                CriterioDepositos = Replace(CriterioDepositos, ")", Empty)
                
                ArrStatus = Split(CriterioDepositos, "|")
                
                SQLDepositos = " AND c_CodDeposito IN ("
                SqlTempDataIn = ""
                
                For i = 0 To UBound(ArrStatus)
                    SqlTempDataIn = SqlTempDataIn & IIf(i < UBound(ArrStatus), _
                    "'" & ArrStatus(i) & "', ", "'" & ArrStatus(i) & "'")
                Next i
                
                SQLDepositos = SQLDepositos & SqlTempDataIn & ")"
            
            ElseIf InStr(CriterioStatus, "EXCEPTO(") > 0 Then
            
                CriterioDepositos = Replace(CriterioDepositos, "EXCEPTO(", Empty)
                CriterioDepositos = Replace(CriterioDepositos, ")", Empty)
                
                ArrStatus = Split(CriterioStatus, "|")
                
                SQLDepositos = " AND c_CodDeposito NOT IN ("
                SqlTempDataIn = ""
                
                For i = 0 To UBound(ArrStatus)
                    SqlTempDataIn = SqlTempDataIn & IIf(i < UBound(ArrStatus), _
                    "'" & ArrStatus(i) & "', ", "'" & ArrStatus(i) & "'")
                Next i
                
                SQLDepositos = SQLDepositos & SqlTempDataIn & ")"
                
            Else
            
                SQLDepositos = ""
            
            End If
            
    End Select
    
    Call ConectarBDD
    
    ENT.BDD.Execute "USE VAD10"
    
    If Not ObtenerMonedaPredeterminada(ENT.BDD) Then
        End
    End If
    
    If Trim(DepositoOrigen) = Trim(DepositoDestino) Then
        LogContent = FormatDateTime(Now, vbGeneralDate) & " - Los depositos origen y destino no pueden ser el mismo."
        LogFile LogContent
        End
    End If
    
    Set RsDeposito = ENT.BDD.Execute("SELECT * FROM MA_DEPOSITO " & _
    "WHERE c_CodDeposito = '" & DepositoOrigen & "' ")
    
    If RsDeposito.EOF Then
        LogContent = FormatDateTime(Now, vbGeneralDate) & " - El dep�sito origen indicado para la solicitud de traslado no existe o no est� definido."
        LogFile LogContent
        End
    End If
    
    RsDeposito.Close
    
    Set RsDeposito = ENT.BDD.Execute("SELECT * FROM MA_DEPOSITO " & _
    "WHERE c_CodDeposito = '" & DepositoDestino & "' ")
    
    If RsDeposito.EOF Then
        LogContent = FormatDateTime(Now, vbGeneralDate) & " - El dep�sito origen indicado para la solicitud de traslado no existe o no est� definido."
        LogFile LogContent
        End
    End If
    
    RsDeposito.Close
    
    mSQLPaso1 = _
    "IF EXISTS(SELECT * FROM TempDB.INFORMATION_SCHEMA.TABLES " & vbNewLine & _
    "WHERE TABLE_NAME LIKE '%#Build_SDT%') " & vbNewLine & _
    "   DROP TABLE #Build_SDT " & vbNewLine
    
    mSQLPaso2 = _
    ";WITH ORDENES AS ( " & vbNewLine & _
    "SELECT (DATEDIFF(DAY, CAST(GETDATE() AS DATE), CAST(d_Fecha_Produccion AS DATE)) + 1) AS DiasRestantesTraslado, OPR.* " & vbNewLine & _
    "FROM MA_ORDEN_PRODUCCION OPR LEFT JOIN MA_CONTROL_AGENTE_ORDEN_PRODUCCION AGT " & vbNewLine & _
    "ON OPR.c_CodLocalidad = AGT.Localidad AND OPR.c_Documento = AGT.Documento_OPR " & vbNewLine & _
    "WHERE c_Status IN ('DPE') AND AGT.Documento_SDT IS NULL " & vbNewLine & _
    "), ProductosEvaluar AS ( " & vbNewLine & _
    "SELECT TR.*, PRODUCTO.nu_StockMin, PRODUCTO.nu_StockMax, " & vbNewLine & _
    "PRODUCTO.n_Impuesto1, PRODUCTO.n_Impuesto2, PRODUCTO.n_Impuesto3, PRODUCTO.Cant_Decimales AS CantDec, " & vbNewLine & _
    "MA.d_Fecha_Produccion, MA.c_Usuario AS CodUsuarioOPR, MA.c_CodMoneda AS MonedaDoc, MA.n_FactorCambio AS FactorDoc " & vbNewLine & _
    "FROM ORDENES MA INNER JOIN TR_ORDEN_PRODUCCION TR " & vbNewLine & _
    "ON MA.c_Documento = TR.c_Documento " & vbNewLine & _
    "AND MA.c_CodLocalidad = TR.c_CodLocalidad " & vbNewLine & _
    "INNER JOIN MA_PRODUCTOS PRODUCTO " & vbNewLine & _
    "ON TR.c_CodArticulo = PRODUCTO.c_Codigo " & vbNewLine & _
    "WHERE DiasRestantesTraslado > 0 And DiasRestantesTraslado <= " & DiasAnticipacion & " And b_Producir = 0 " & vbNewLine & _
    "), DepoProd AS ( " & vbNewLine & _
    "SELECT ROUND(isNULL((DP.n_Cantidad - CASE WHEN DP.n_Cant_Comprometida " & vbNewLine & _
    "> 0 THEN DP.n_Cant_Comprometida ELSE 0 END), 0), 8, 0) AS Existencia, " & vbNewLine & _
    "DP.c_CodDeposito, PRO.* FROM ProductosEvaluar PRO LEFT JOIN MA_DEPOPROD DP " & vbNewLine & _
    "ON PRO.c_CodArticulo = DP.c_CodArticulo " & vbNewLine
    
    mSQLPaso2 = mSQLPaso2 & _
    "), RequeridosPorDocumento AS ( " & vbNewLine & _
    "SELECT c_CodLocalidad, c_Documento, c_CodArticulo, CantDec, d_Fecha_Produccion, MonedaDoc, FactorDoc, " & vbNewLine & _
    "ROUND(SUM(n_Cantidad), CantDec, 0) AS Requeridos, " & vbNewLine & _
    "ROUND((isNULL(SUM(Existencia), 0) * (" & FormatNumber(PorcentajeExistencia, 2) & " / 100.0))" & IIf(ConsideraStockMinimo, " - nu_StockMin", Empty) & ", CantDec, 0) AS Existencia, " & vbNewLine & _
    "ROW_NUMBER() OVER (PARTITION BY c_CodArticulo ORDER BY d_Fecha_Produccion, c_CodArticulo) AS Prioridad, " & vbNewLine & _
    "ROW_NUMBER() OVER (ORDER BY c_CodArticulo) AS RowID " & vbNewLine & _
    "FROM DEPOPROD " & vbNewLine & _
    "WHERE 1 = 1 " & SQLDepositos & vbNewLine & _
    "GROUP BY c_CodLocalidad, c_Documento, c_CodArticulo, CantDec, d_Fecha_Produccion, nu_StockMin, MonedaDoc, FactorDoc " & vbNewLine & _
    "), CALC AS ( " & vbNewLine & _
    "SELECT " & vbNewLine & _
    "RowID, c_CodLocalidad, c_Documento, c_CodArticulo, d_Fecha_Produccion, Requeridos, Existencia, Prioridad, " & vbNewLine & _
    "CASE WHEN Existencia > Requeridos THEN ROUND(Existencia - Requeridos, CantDec, 0) ELSE 0 END AS ExistenciaRestante, " & vbNewLine & _
    "CASE WHEN (Requeridos > Existencia) THEN ROUND(Requeridos - Existencia, CantDec, 0) ELSE 0 END AS Faltantes " & vbNewLine & _
    "FROM RequeridosPorDocumento WHERE Prioridad = 1 " & vbNewLine & _
    "UNION ALL " & vbNewLine
    
    mSQLPaso2 = mSQLPaso2 & _
    "SELECT " & vbNewLine & _
    "RequeridosPorDocumento.RowID, RequeridosPorDocumento.c_CodLocalidad, RequeridosPorDocumento.c_Documento, RequeridosPorDocumento.c_CodArticulo, RequeridosPorDocumento.d_Fecha_Produccion, " & vbNewLine & _
    "RequeridosPorDocumento.Requeridos, RequeridosPorDocumento.Existencia, RequeridosPorDocumento.Prioridad, " & vbNewLine & _
    "(CASE WHEN CALC.ExistenciaRestante > 0 THEN ROUND(CALC.ExistenciaRestante - " & vbNewLine & _
    "(CASE WHEN CALC.ExistenciaRestante > RequeridosPorDocumento.Requeridos THEN " & vbNewLine & _
    "RequeridosPorDocumento.Requeridos ELSE CALC.ExistenciaRestante END) " & vbNewLine & _
    ", RequeridosPorDocumento.CantDec, 0) ELSE 0 END) AS ExistenciaRestante, " & vbNewLine & _
    "CASE WHEN CALC.ExistenciaRestante > 0  THEN " & vbNewLine & _
    "CASE WHEN (RequeridosPorDocumento.Requeridos > CALC.ExistenciaRestante) THEN " & vbNewLine & _
    "ROUND(RequeridosPorDocumento.Requeridos - CALC.ExistenciaRestante, CantDec, 0) ELSE 0 END " & vbNewLine & _
    "ELSE RequeridosPorDocumento.Requeridos END AS Faltantes " & vbNewLine & _
    "FROM CALC INNER JOIN RequeridosPorDocumento " & vbNewLine & _
    "ON RequeridosPorDocumento.RowID = CALC.RowID + 1 AND RequeridosPorDocumento.Prioridad > 1 " & vbNewLine & _
    "), Build_SDT_1 AS ( " & vbNewLine & _
    "SELECT c_Documento AS Doc_ID, ROW_NUMBER() OVER (ORDER BY c_Documento) AS SDT_ID " & vbNewLine & _
    "FROM CALC GROUP BY c_Documento " & vbNewLine
    
    If ConsiderarExistencias Then
        mCantidadSolicitar = "ROUND(SDT2.Requeridos - SDT2.Faltantes, Items.CantDec, 0)"
    Else
        mCantidadSolicitar = "ROUND(SDT2.Requeridos, Items.CantDec, 0)"
    End If
    
    mSQLPaso2 = mSQLPaso2 & _
    "), Build_SDT_2 AS ( " & vbNewLine & _
    "SELECT CorrelativoSDT = RIGHT('0000000000000000' + CAST((nu_Valor + SDT_ID) AS NVARCHAR(MAX)), 9), " & vbNewLine & _
    "CALC.*, SDT.SDT_ID " & vbNewLine & _
    "FROM VAD10..MA_CORRELATIVOS CROSS JOIN CALC INNER JOIN Build_SDT_1 SDT " & vbNewLine & _
    "ON CALC.c_Documento = SDT.Doc_ID " & vbNewLine & _
    "WHERE cu_Campo = 'REQUISICIONDEPOSITO' " & vbNewLine & _
    ") SELECT ROW_NUMBER() OVER (PARTITION BY SDT2.c_Documento ORDER BY SDT2.RowID) AS SDT_Linea, SDT2.SDT_ID, SDT2.CorrelativoSDT, SDT2.ExistenciaRestante, SDT2.Faltantes, " & vbNewLine & _
    mCantidadSolicitar & " AS Solicitar, Items.* " & vbNewLine & _
    "INTO #Build_SDT FROM Build_SDT_2 SDT2 " & vbNewLine & _
    "INNER JOIN ProductosEvaluar Items " & vbNewLine & _
    "ON Items.c_CodArticulo = SDT2.c_CodArticulo " & vbNewLine & _
    "AND Items.c_CodLocalidad = SDT2.c_CodLocalidad " & vbNewLine & _
    "AND Items.c_Documento = SDT2.c_Documento " & vbNewLine & _
    "WHERE " & mCantidadSolicitar & " > 0 " & vbNewLine & _
    "ORDER BY SDT2.RowID " & vbNewLine & _
    "OPTION (MAXRECURSION 9999) " & vbNewLine & _
    "; " & vbNewLine
    
    mSQLPaso3 = _
    "DECLARE @CantSDT INT " & vbNewLine & _
    "SET @CantSDT = (SELECT isNULL(COUNT(*), 0) AS Cant FROM #Build_SDT) " & vbNewLine
    
    mSQLPaso3 = mSQLPaso3 & _
    "IF (@CantSDT > 0) " & vbNewLine & _
    "BEGIN " & vbNewLine & _
    "" & vbNewLine & _
    "    BEGIN TRANSACTION " & vbNewLine & _
    "    " & vbNewLine & _
    "    INSERT INTO TR_REQUISICION_DEPOSITO " & vbNewLine & _
    "    (CS_DOCUMENTO, CS_ESTADO, NS_LINEA, CS_CODARTICULO, CS_CODIGO_PROVEEDOR, CS_CODEDI, CS_DESCRIPCION, " & vbNewLine & _
    "    NS_CANTIDAD, NS_COSTO, NS_CANT_RECIBIDA, NS_DECIMALES, CS_CODMONEDA, NS_FACTOR_CAMBIO, NS_PROD_EXT, " & vbNewLine & _
    "    cs_codlocalidad, ns_CantidadEmpaque) " & vbNewLine & _
    "    SELECT CorrelativoSDT, 'DPE', SDT.SDT_Linea, SDT.c_CodArticulo, '', SDT.c_CodArticulo, SDT.c_Descripcion, " & vbNewLine & _
    "    SDT.Solicitar, SDT.n_Costo, 0, SDT.CantDec, SDT.c_MonedaProd, SDT.n_FactorMonedaProd, SDT.n_Prod_Ext, " & vbNewLine & _
    "    SDT.c_CodLocalidad, SDT.ns_CantidadEmpaque " & vbNewLine & _
    "    FROM #Build_SDT SDT ORDER BY CorrelativoSDT, SDT.c_CodArticulo " & vbNewLine
    
    mSQLPaso3 = mSQLPaso3 & _
    "    INSERT INTO MA_REQUISICION_DEPOSITO " & vbNewLine & _
    "    (CS_DOCUMENTO, DS_FECHA, CS_ESTADO, CS_CODLOCALIDAD, CS_OBSERVACION, CS_RELACION, CS_CODOPERADOR, DS_FECHA_TOPE, " & vbNewLine & _
    "    CS_DEPOSITO_SOLICITUD, CS_DEPOSITO_DESPACHAR, NU_ESTADO_INTERNO, NU_PRIORIDAD, CS_CODMONEDA, NS_FACTOR_CAMBIO) " & vbNewLine & _
    "    SELECT TOTALES.Doc, CAST(GETDATE() AS DATE), 'DPE', TOTALES.Loc, " & vbNewLine & _
    "    'SOLICITUD DE TRASLADO GENERADA AUTOMATICAMENTE POR AGENTE DE ORDENES DE PRODUCCI�N N� ' + TOTALES.DocOrigen, " & vbNewLine & _
    "    '', TOTALES.CodUsuarioOPR, CAST(GETDATE() AS DATE), '" & DepositoOrigen & "', '" & DepositoDestino & "', 0, 10, TOTALES.MonedaDoc, TOTALES.FactorDoc FROM " & vbNewLine & _
    "    (SELECT TR2.cs_CodLocalidad AS Loc, TR2.CS_DOCUMENTO AS Doc, SDT2.c_Documento AS DocOrigen, SDT2.CodUsuarioOPR, SDT2.MonedaDoc, SDT2.FactorDoc " & vbNewLine & _
    "    FROM TR_REQUISICION_DEPOSITO TR2 INNER JOIN #Build_SDT SDT2 " & vbNewLine & _
    "    ON TR2.CS_DOCUMENTO = SDT2.CorrelativoSDT " & vbNewLine & _
    "    AND TR2.CS_CODARTICULO = SDT2.c_CodArticulo " & vbNewLine & _
    "    GROUP BY TR2.cs_CodLocalidad, TR2.CS_DOCUMENTO, SDT2.c_Documento, SDT2.CodUsuarioOPR, SDT2.MonedaDoc, SDT2.FactorDoc) TOTALES " & vbNewLine & _
    "    ORDER BY TOTALES.Doc " & vbNewLine
    
    mSQLPaso3 = mSQLPaso3 & _
    "" & vbNewLine & _
    "    UPDATE VAD10..MA_CORRELATIVOS SET nu_Valor = (nu_Valor + @CantSDT)  WHERE cu_Campo = 'REQUISICIONDEPOSITO' " & vbNewLine & _
    "" & vbNewLine & _
    "    INSERT INTO MA_CONTROL_AGENTE_ORDEN_PRODUCCION " & vbNewLine & _
    "    (Localidad, Documento_OPR, Documento_SDC, Documento_SDT) " & vbNewLine & _
    "    SELECT c_CodLocalidad, c_Documento, NULL, NULL " & vbNewLine & _
    "    FROM ( " & vbNewLine & _
    "    SELECT c_CodLocalidad, c_Documento FROM #Build_SDT " & vbNewLine & _
    "    GROUP BY c_CodLocalidad, c_Documento) OPR " & vbNewLine & _
    "    LEFT JOIN MA_CONTROL_AGENTE_ORDEN_PRODUCCION AGT " & vbNewLine & _
    "    ON OPR.c_CodLocalidad = AGT.Localidad " & vbNewLine & _
    "    AND OPR.c_Documento = AGT.Documento_OPR " & vbNewLine & _
    "    WHERE AGT.Documento_OPR IS NULL " & vbNewLine & _
    "    ORDER BY c_CodLocalidad, c_Documento " & vbNewLine
    
    mSQLPaso3 = mSQLPaso3 & _
    "" & vbNewLine & _
    "    UPDATE MA_CONTROL_AGENTE_ORDEN_PRODUCCION " & vbNewLine & _
    "    SET Documento_SDT = CorrelativoSDT " & vbNewLine & _
    "    FROM ( " & vbNewLine & _
    "    SELECT c_CodLocalidad, c_Documento, CorrelativoSDT FROM #Build_SDT " & vbNewLine & _
    "    GROUP BY c_CodLocalidad, c_Documento, CorrelativoSDT) OPR " & vbNewLine & _
    "    INNER JOIN MA_CONTROL_AGENTE_ORDEN_PRODUCCION AGT " & vbNewLine & _
    "    ON OPR.c_CodLocalidad = AGT.Localidad " & vbNewLine & _
    "    AND OPR.c_Documento = AGT.Documento_OPR " & vbNewLine & _
    "     " & vbNewLine & _
    "    COMMIT TRANSACTION " & vbNewLine & _
    "" & vbNewLine & _
    "END " & vbNewLine
    
    ENT.BDD.CommandTimeout = 0

    Dim TInicio As Date, TFin As Date
    
    'ENT.BDD.BeginTrans
    
    TInicio = Now
    
    'Trans = True
    
    ENT.BDD.Execute mSQLPaso1
    ENT.BDD.Execute mSQLPaso2
    ENT.BDD.Execute mSQLPaso3
    
    'ENT.BDD.CommitTrans
    
    TFin = Now
    
    Debug.Print "Tiempo de actualizacion: " & FormatDateTime(CDate(TFin - TInicio), vbLongTime)
    
    Exit Sub
    
Error1:
    
    If Trans Then
        ENT.BDD.RollbackTrans
    End If
    
    LogContent = FormatDateTime(Now, vbGeneralDate) & " - Error al Generar Solicitudes de Traslado: " & Err.Description & " " & "(" & Err.Number & ")[" & Err.Source & "]"
    LogFile LogContent
    
End Sub

Public Function BuscarValorBD(Campo As String, sql As String, Optional pDefault = "", Optional pCn As ADODB.Connection) As String

On Error GoTo Err_Campo

Dim mRs As New ADODB.Recordset

mRs.Open sql, IIf(pCn Is Nothing, ENT.BDD, pCn), adOpenStatic, adLockReadOnly, adCmdText

If Not mRs.EOF Then

    BuscarValorBD = CStr(mRs.Fields(Campo).Value)

Else
    
    BuscarValorBD = pDefault

End If

mRs.Close

Exit Function

Err_Campo:
Debug.Print Err.Description
Err.Clear
BuscarValorBD = pDefault

End Function

Public Function mensaje(activo As Boolean, texto As String, Optional pVbmodal = True) As Boolean
    uno = activo
    On Error GoTo Falla_Local
    
    If Not frm_MENSAJERIA.Visible Then
        frm_MENSAJERIA.mensaje.Text = IIf(frm_MENSAJERIA.mensaje.Text <> "", frm_MENSAJERIA.mensaje.Text & vbNewLine & vbNewLine & texto, texto)
        If pVbmodal Then
            If Not frm_MENSAJERIA.Visible Then
                Retorno = False
                
                frm_MENSAJERIA.Show vbModal
                Set frm_MENSAJERIA = Nothing
            End If
        Else
            Retorno = False
            frm_MENSAJERIA.Show
            frm_MENSAJERIA.aceptar.Enabled = False
            Set frm_MENSAJERIA = Nothing
        End If
    End If
    mensaje = Retorno
Falla_Local:
End Function

Public Function ObtenerMonedaPredeterminada(ByVal pCn As Variant) As Boolean
    
    On Error GoTo Error
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    mSQL = "SELECT * FROM VAD10.DBO.MA_MONEDAS " & _
    "WHERE b_Preferencia = 1"
    
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        
        Moneda_Cod = mRs!c_CodMoneda
        Moneda_Fac = mRs!n_Factor
        Moneda_Des = mRs!c_Descripcion
        Moneda_Dec = mRs!n_Decimales
        Moneda_Sim = mRs!c_Simbolo
        
        ObtenerMonedaPredeterminada = True
        
    Else
        
        ObtenerMonedaPredeterminada = False
        LogFile "No existe una moneda predeterminada en el sistema. Por favor revise la configuraci�n en ficha de monedas.", 999
        
    End If
    
    mRs.Close
    
    Exit Function
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    ObtenerMonedaPredeterminada = False
    
    LogContent = FormatDateTime(Now, vbGeneralDate) & " - Error al buscar moneda predeterminada. Informacion adicional: " & mErrDesc & " " & "(" & mErrNumber & ")[" & mErrSrc & "]"
    LogFile LogContent
    
End Function
